*** Settings ***
Resource    ../Keywords/ChatGPT.resource
Resource    ../Keywords/General.resource
Library     ../Libraries/ppt_helper.py

Suite Setup     Bepaal OS


*** Variables ***
${CHAT_URL}=            https://api.openai.com
${PPTX_RF_NAAM}=        AntwoordVanChatGptRf.pptx
${PPTX_KVK_NAAM}=       AntwoordVanChatGptKvk.pptx
${PPTX_RR_NAAM}=        AntwoordVanChatGptRr.pptx


*** Test Cases ***
Voorbeeld ChatGPT 1
    ${antwoord} =    Stel Vraag Aan ChatGPT    geef een puntsgewijze beschrijving van de testtool robotframework
    Log    ${antwoord}
    Log To Console    ${antwoord}
    Genereer Presentatie   Robot Framework   ${PPTX_RF_NAAM}    ${antwoord}[0]
    Open Presentatie    ${PPTX_RF_NAAM}

Voorbeeld ChatGPT 2
    ${antwoord}=    Stel Vraag Aan ChatGPT      wat doet de KVK in Nederland
    Genereer Presentatie   KVK   ${PPTX_KVK_NAAM}    ${antwoord}[0]
    Open Presentatie    ${PPTX_KVK_NAAM}

Voorbeeld ChatGPT 3
    ${antwoord}=    Stel Vraag Aan ChatGPT      geef een puntsgewijs overzicht van het leven de de belangrijkste werken van Rembrandt van Rijn de schilder
    Genereer Presentatie   RR   ${PPTX_RR_NAAM}    ${antwoord}[0]
    Open Presentatie    ${PPTX_RR_NAAM}
