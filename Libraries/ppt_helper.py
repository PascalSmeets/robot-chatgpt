from pptx import Presentation
from pptx.util import Cm, Pt
from pptx.enum.text import MSO_AUTO_SIZE
from robot.api.deco import keyword


@keyword
def genereer_presentatie(titel, filenaam, bodytekst):
    lines = bodytekst.splitlines()
    lines = list(filter(None, lines))

    X = Presentation()
    Layout = X.slide_layouts[0]
    title_slide = X.slides.add_slide(Layout)  # Adding first slide
    title_slide.shapes.title.text = titel + " volgens ChatGPT [gpt-3.5-turbo]"
    subtitle = title_slide.placeholders[1]
    subtitle.text = "...neem dit dus met een korreltje zout..."

    Second_Layout = X.slide_layouts[6]

    for line in lines:
        new_slide = X.slides.add_slide(Second_Layout)  # Adding next slide
        textbox = new_slide.shapes.add_textbox(
            left=Cm(2), top=Cm(2), width=Cm(20), height=Cm(10)
        )

        textframe = textbox.text_frame
        textframe.autofit_text = True
        textframe.margin_bottom = Cm(1)
        textframe.margin_left = 0
        textframe.word_wrap = True
        textframe.auto_size = MSO_AUTO_SIZE.TEXT_TO_FIT_SHAPE
        paragraph = textframe.add_paragraph()
        run = paragraph.add_run()
        font = run.font
        font.name = "Calibri"
        font.size = Pt(30)
        font.bold = True
        font.italic = None  # cause value to be inherited from theme

        run.text = line

    X.save(filenaam)
